<?php

namespace App\Controller;

use App\Entity\Products;
use App\Entity\PropertySearch;
use App\Form\PropertySearchType;
use App\Repository\ProductsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductsController extends AbstractController
{
 

    /**
     * @Route("/", name="product_index")
     */
    public function index(ProductsRepository $productsRepository,Request $request)
    {
        $propertySearch = new PropertySearch();
        $form = $this->createForm(PropertySearchType::class,$propertySearch);
            $form->handleRequest($request);
        //initialement le tableau des articles est vide,
            //c.a.d on affiche les articles que lorsque l'utilisateur
            //clique sur le bouton rechercher
            $Products= $this->getDoctrine()->getRepository(Products::class)->findAll();
 
            if($form->isSubmitted() && $form->isValid()) {
    //on récupère le nom d'article tapé dans le formulaire$nom = $propertySearch->getNom(); 
        $nom=$propertySearch->getNom();
        if ($nom!="")
            //si on a fourni un nom d'article on affiche tous les articles ayant ce nom
            $Products= $this->getDoctrine()->getRepository(Products::class)->findByMot($nom);
            
       
        }
         return $this->render('products/index.html.twig',[ 'form' => $form-> createView() , 'Products' => $Products]); 
          }

   

 /**
 * @Route("/article/save")
 */
public function save() {
    $entityManager = $this->getDoctrine()->getManager();
    $Produits = new Produits();
    $Produits->setTitle('Article 1');
    $Produits->setImage('ps4.png');
    $Produits->setPrice(1000);
    
    $entityManager->persist($Produits);
    $entityManager->flush();
    return new Response('Article enregisté avec id '.$Produits->getId());
    }
   

 /**
 * @Route("/article/new", name="new_article")
 * Method({"GET", "POST"})
 */
public function new(Request $request) {
    $Produits = new Products();
    $form = $this->createFormBuilder($Produits)->add('title', TextType::class)->add('image', TextType::class)->add('price', TextType::class)->add('save', SubmitType::class, array(
    'label' => 'Créer'))->getForm();
 
 
    $form->handleRequest($request);
    
    if($form->isSubmitted() && $form->isValid()) {
        $Produits = $form->getData();
    
    $entityManager = $this->getDoctrine()->getManager();
    $entityManager->persist($Produits);
    $entityManager->flush();
    
    return $this->redirectToRoute('article_list');
    }
    return $this->render('products/new.html.twig',['form' => $form->createView()]);
    }

    /**
 * @Route("/article/{id}", name="article_show")
 */
 public function show($id) {
    $Produits = $this->getDoctrine()->getRepository(Article::class)->find($id);
    return $this->render('Products/show.html.twig',
    array('$Produits' => $Produits));
     }
   
   


}
